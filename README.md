# Lilypond ParGen

Rust library for parsing and generating Lilypond music notation.

## Generating Lilypond

The types used are able to encode certain values that are not valid Lilypond, i.e. they are not completely tight. One common example of this is note values that do not have 1 (normal note) or 3 (dotted note) or 7 (double dotted note) in the numerator or a power of 2 in the denominator, or are crossing over barlines. This makes them easier to use for composing, but requires an extra step before exporting to lilypond. A series of `LPNode` has to be split by calling `split_compound_durations()` on the container node, which will recursively split everything with a duration in ways that enable correct Lilypond generation.


## Microtonal notation resources

### Custom key signatures

https://music.stackexchange.com/questions/85189/lilypond-create-natural-signs-and-accidentals-manually

``` lilypond
\score{
    {
        \set Staff.keySignature = #`((6 . ,FLAT) (3 . ,SHARP))
        \override Staff.KeySignature.stencil = #ly:text-interface::print
        \override Staff.KeySignature.text =
        \markup {
          \concat {
            \musicglyph #"accidentals.flat"
            \lower #0.4 \super \bold \fontsize #-3 2
          }
          \concat {
            \raise #2 \musicglyph #"accidentals.sharp"
            \raise #1.6 \super \bold \fontsize #-3 3
          }
        }
        \time 9/8
        \relative c' {
            s8 fis bes s4. fis!8 bes! s8 \bar "|."
        }
    }
    \layout{}
}

```

### HEWM notation

It seems like the accidentals are available, but note names for them have to be defined by the user through long lists. Here is a recently added symbol (v2.22):
https://lilypond.org/doc/v2.22/Documentation/changes/

Here is an example of some custom accidentals:
https://lsr.di.unimi.it/LSR/Snippet?id=786

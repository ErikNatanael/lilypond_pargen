use fraction::Fraction;
use fraction::ToPrimitive;
use fraction::Zero;
use rand::{seq::SliceRandom, thread_rng, Rng};
use std::{collections::HashMap, fmt, fs::File, io::Write};

use crate::parser::split_compound_duration_notes;

/// Represents a full Lilypond document. Can be generated from a Lilypond file using the `Parser`
#[derive(Clone, Debug)]
pub struct LPDocument {
    version: String,
    pub score: LPNode,
    pub header: LPHeader,
    pub paper: Vec<LPProperty>,
    // TODO: layout, midi
}

impl LPDocument {
    pub fn from_score(score: LPNode) -> Self {
        let version = String::from("2.20.0");
        let header = LPHeader::new();
        Self {
            version,
            score,
            header,
            paper: vec![],
        }
    }
    pub fn stringify(&self) -> String {
        let mut out = String::new();
        // Add version
        out.push_str(&format!("\\version \"{}\"\n", self.version));
        out.push_str(&self.header.stringify());
        out.push_str("\\book { \n");
        out.push_str("\\score { \n");
        out.push_str(&self.score.stringify());
        out.push_str("\n\\layout { } ");
        out.push_str("\n\\midi { } ");
        out.push_str("\n} "); // End score
        // Paper
        out.push_str("\n\\paper { \n");
        for prop in &self.paper {
            out.push_str(&prop.stringify());
        }
        out.push_str("\n }\n");
        out.push_str("\n} "); // End book
        out
    }
    /// Compile the lilypond and open the resulting pdf right away. Is
    /// only expected to work on Linux
    pub fn open_as_pdf(&self, play_midi: bool) {
        // Write a temporary file that the lilypond compiler can compile
        let temp_directory = std::env::temp_dir();
        let temp_file = temp_directory.join("lilypond_tmp.ly");
        let temp_out_file = temp_directory.join("lilypond_tmp");
        let mut file = File::create(&temp_file).unwrap();
        file.write_all(self.stringify().as_bytes())
            .expect("write failed");

        // Run the lilypond compiler on this new file
        let status = std::process::Command::new("lilypond")
            .arg("--output")
            .arg(temp_out_file.to_str().unwrap())
            .arg(temp_file.to_str().unwrap())
            .status()
            .expect("failed to execute process");

        if status.success() {
            // Open the pdf file with the standard pdf reader on the system
            let temp_pdf_file = temp_directory.join("lilypond_tmp.pdf");
            std::process::Command::new("xdg-open")
                .arg(temp_pdf_file.to_str().unwrap())
                .status()
                .expect("failed to execute process");
            // std::process::Command::new("zathura")
            //     .arg("--mode")
            //     .arg("fullscreen")
            //     .arg(temp_pdf_file.to_str().unwrap())
            //     .status()
            //     .expect("failed to execute process");

            if play_midi {
                let temp_midi_file = temp_directory.join("lilypond_tmp.midi");

                std::process::Command::new("fluidsynth")
                    .arg("--audio-driver=jack")
                    .arg("--connect-jack-outputs")
                    // Would be better not to have this hardcoded...
                    .arg("/home/erik/soundfonts/MuseScore_General.sf3")
                    .arg(temp_midi_file.to_str().unwrap())
                    .spawn()
                    .expect("failed to execute process");
            }
        }
    }
}

/// TODO: This should probably be an LPNode variant instead because it can be at four different levels in a document: top level, inside \book, inside \bookpart, inside \score
/// More info on the different strings: https://lilypond.org/doc/v2.23/Documentation/notation/creating-titles-headers-and-footers#default-layout-of-bookpart-and-score-titles
#[derive(Clone, Debug)]
pub struct LPHeader {
    pub dedication: Option<String>,
    pub title: Option<String>,
    pub subtitle: Option<String>,
    pub instrument: Option<String>,
    pub composer: Option<String>,
    pub pdftitle: Option<String>,
    pub copyright: Option<String>,
    pub tagline: Option<String>,
    pub piece: Option<String>,
    pub opus: Option<String>,
}
impl LPHeader {
    pub fn new() -> Self {
        LPHeader {
            dedication: None,
            title: None,
            subtitle: None,
            instrument: None,
            composer: None,
            pdftitle: None,
            copyright: None,
            tagline: None,
            piece: None,
            opus: None,
        }
    }
    pub fn stringify(&self) -> String {
        let mut out = String::from("\\header {\n");
        if let Some(dedication) = &self.dedication {
            out.push_str(&format!("dedication = \"{}\"\n", dedication));
        }
        if let Some(title) = &self.title {
            out.push_str(&format!("title = \"{}\"\n", title));
        }
        if let Some(subtitle) = &self.subtitle {
            out.push_str(&format!("subtitle = \"{}\"\n", subtitle));
        }
        if let Some(instrument) = &self.instrument {
            out.push_str(&format!("instrument = \"{}\"\n", instrument));
        }
        if let Some(composer) = &self.composer {
            out.push_str(&format!("composer = \"{}\"\n", composer));
        }
        if let Some(pdftitle) = &self.pdftitle {
            out.push_str(&format!("pdftitle = \"{}\"\n", pdftitle));
        }
        if let Some(copyright) = &self.copyright {
            out.push_str(&format!("copyright = \"{}\"\n", copyright));
        }
        if let Some(tagline) = &self.tagline {
            out.push_str(&format!("tagline = \"{}\"\n", tagline));
        }
        if let Some(piece) = &self.piece {
            out.push_str(&format!("piece = \"{}\"\n", piece));
        }
        if let Some(opus) = &self.opus {
            out.push_str(&format!("opus = \"{}\"\n", opus));
        }
        out.push_str("}\n");
        out
    }
}

#[derive(Clone, Debug)]
pub struct PositionInPiece {
    pub raw_position: Fraction, // raw length into piece in terms of
    pub bar_number: u32,
    pub bar_position: Fraction, // position relative to the bar it's in
}

impl PositionInPiece {
    pub fn new() -> Self {
        PositionInPiece {
            raw_position: Fraction::zero(),
            bar_number: 0,
            bar_position: Fraction::zero(),
        }
    }
    pub fn from(rl: Fraction, bn: u32, bp: Fraction) -> Self {
        PositionInPiece {
            raw_position: rl,
            bar_number: bn,
            bar_position: bp,
        }
    }
}

pub fn note_name_to_midi_note(name: &str, octave: i8) -> Result<f32, String> {
    // todo: add more variants e.g. fes, beses, ceh, cih
    // TODO: make the HashMap static
    let note_names = vec![
        "c", "cis", "des", "d", "dis", "es", "e", "eis", "f", "fis", "ges", "g", "gis", "as", "a",
        "ais", "bes", "b",
    ];
    let note_numbers = vec![0, 1, 1, 2, 3, 3, 4, 5, 5, 6, 6, 7, 8, 8, 9, 10, 10, 11];
    let note_map: HashMap<_, _> = note_names.iter().zip(note_numbers.iter()).collect();

    match note_map.get(&name) {
        Some(note) => Ok((*note + 48 + 12 * octave as i32) as f32),
        None => Err(format!(
            "Note not found in list, could not be parsed: \"{}\", {}",
            name, octave
        )),
    }
}

pub fn midi_note_to_name(note: f32) -> (String, i8) {
    let note_names = vec![
        "c", "cis", "d", "es", "e", "f", "fis", "g", "gis", "a", "bes", "b",
    ];
    let note_numbers = vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
    let note_map: HashMap<_, _> = note_numbers.iter().zip(note_names.iter()).collect();
    // middle c  which is octave 0 here has midi note number 48
    // The note must be floored to support negative number octaves
    let octave = ((note - 48.0) / 12.0f32).floor() as i8;
    let name = note_map
        // Note: % gives the signed remainder, like C. `rem_euclid()` does what we want which is to give us "the least nonnegative remainder". (-13).rem_euclid == 11
        .get(&(note as i32).rem_euclid(12))
        .expect("Could not find note name");
    (String::from(**name), octave)
}

#[derive(Clone, Debug)]
pub struct TimeSignature {
    pub numerator: u32,
    pub denominator: u32,
    pub groupings: Vec<u32>, // e.g. [3, 3] for 6/8 or [4, 3] for 7/8
}

impl TimeSignature {
    pub fn from(n: u32, d: u32) -> Self {
        // default groupings, to be developed
        let mut groupings = vec![];
        if n % 2 == 0 {
            groupings.push(n / 2);
            groupings.push(n / 2);
        } else {
            groupings.push(n / 2);
            // counting on the fractional part being discarded in integer division
            groupings.push(n / 2 + 1);
        }
        TimeSignature {
            numerator: n,
            denominator: d,
            groupings,
        }
    }
    pub fn is_full_bar(&self, nl: &Fraction) -> bool {
        // self.as_f32() == nl.to_f32().unwrap()
        // Comparing floats directly may be risky due to rounding errors.
        // Below is equivalent to a comparison with a very small margin of error
        (self.as_f32() - nl.to_f32().unwrap()).abs() < f32::EPSILON
    }
    pub fn as_f32(&self) -> f32 {
        self.numerator as f32 / self.denominator as f32
    }
    pub fn to_fraction(&self) -> Fraction {
        Fraction::new(self.numerator as u64, self.denominator as u64)
    }
    // Returns the number of ticks in a bar of this time signature based on the points per quarter note.
    pub fn ticks_per_bar(&self, ppqn: u32) -> i64 {
        ((self.numerator * ppqn * 4) / self.denominator) as i64
    }
}

/// "Context properties are properties that apply to the context as a
/// whole and controlhow the context itself is displayed."
///
/// "context settings can appear in one of three locations within our
/// input file – in a \with block, in a \context block, or directly in
/// music entry preceded by the \set command"
#[derive(Clone, Debug)]
pub struct LPProperty {
    property: String,
    argument: String, // context properties can have different types, strings much be enclosed in quotation marks
}
impl LPProperty {
    pub fn new(property: impl ToString, argument: impl ToString) -> Self {
        Self {
            property: property.to_string(),
            argument: argument.to_string(),
        }
    }
    pub fn stringify(&self) -> String {
        format!("{} = {} ", self.property, self.argument)
    }
    pub fn midi_instrument<T: AsRef<str>>(instrument: T) -> Self {
        // If the instrument name isn't enclosed in quotation marks they are added
        let argument = enclose_in_quotation(instrument.as_ref().to_owned());
        Self {
            property: String::from("midiInstrument"),
            argument,
        }
    }
    pub fn instrument_name<T: AsRef<str>>(instrument: T) -> Self {
        // If the instrument name isn't enclosed in quotation marks they are added
        let argument = enclose_in_quotation(instrument.as_ref().to_owned());
        Self {
            property: String::from("instrumentName"),
            argument,
        }
    }
}

pub fn enclose_in_quotation(s: String) -> String {
    if s.starts_with('"') {
        s
    } else {
        format!("\"{}\"", s)
    }
}

/// https://lilypond.org/doc/v2.23/Documentation/notation-big-page.html#index-_005ctempo-1
/// Different uses in lilypond:
/// * \tempo 4=96                    % Metronome mark
/// * \tempo 4 = 120                 % Metronome mark
/// * \tempo 4 = 40 - 46             % Metronome range
/// * \tempo "Allegretto"            % Text
/// * \tempo "Allegro" 4 = 160       % Text and metronome mark
/// * \tempo \markup { \italic Faster } 4 = 132 % Markup object and metronome mark
/// * \tempo "" 8 = 96               % Metronome mark in parentheses
///
/// Only text OR markup is allowed. This could be expressed with an
/// enum, but the number of enums is starting to get ridiculous. If
/// text is Some, the markup_object will be ignored.
#[derive(Clone, Debug)]
pub struct LPTempo {
    text: Option<String>,
    markup_object: Option<LPMarkupObject>,
    metronome_mark: Option<LPMetronomeMark>,
}
impl LPTempo {
    pub fn stringify(&self) -> String {
        let mut out = String::from("\\tempo ");
        if let Some(ref text) = self.text {
            out.push_str(&format!("\"{}\"", text));
        } else if let Some(ref markup) = self.markup_object {
            out.push_str(&markup.stringify());
        }
        if let Some(ref metronome_mark) = self.metronome_mark {
            out.push_str(&metronome_mark.stringify());
        }
        out
    }
}

#[derive(Clone, Debug)]
pub enum LPMetronomeMark {
    Fixed {
        note_value: u32,
        bpm: u32,
    },
    Range {
        note_value: u32,
        start_bpm: u32,
        end_bpm: u32,
    },
}
impl LPMetronomeMark {
    pub fn stringify(&self) -> String {
        match self {
            LPMetronomeMark::Fixed { note_value, bpm } => {
                format!("{} = {} ", note_value, bpm)
            }
            LPMetronomeMark::Range {
                note_value,
                start_bpm,
                end_bpm,
            } => {
                format!("{} = {} - {} ", note_value, start_bpm, end_bpm)
            }
        }
    }
}

///  A \markup block may contain many
/// kinds of LPNode objects and may be attacked to notes, tempo markings among others:
/// * https://lilypond.org/doc/v2.23/Documentation/notation-big-page.html#music-notation-inside-markup
///
#[derive(Clone, Debug)]
pub struct LPMarkupObject {
    nodes: Vec<LPNode>,
}
impl LPMarkupObject {
    pub fn stringify(&self) -> String {
        let mut out = String::from("\\markup { ");
        for node in &self.nodes {
            out.push_str(&node.stringify());
        }
        out.push_str("} ");
        out
    }
}

#[derive(Clone, Debug)]
pub enum LPCommand {
    /// \new can be followed by Staff, Voice, PianoStaff or e.g. '\new Voice = "melody"', usually followed by a SequentialExpression or a ParallelExpression or a \with command
    New {
        kind: String,
        argument: Option<String>,
    },
    /// With is setting "context properties" and must come straight after a \new command or a short form input mode such as "\chords"
    With(Vec<LPProperty>),
    /// Override is used to change the properties of layout objects
    /// https://lilypond.org/doc/v2.23/Documentation/learning/tweaking-methods#the-override-command
    Override(LPProperty),
    /// There are tons of different clefs supported
    Clef(String),
    Tempo(LPTempo),
    /// Commands which don't need special handling
    Other(String),
}

impl LPCommand {
    pub fn stringify(&self) -> String {
        match self {
            LPCommand::New { kind, argument } => {
                let mut out = String::new();
                out.push_str(&format!("\\new {} ", kind));
                if let Some(arg) = argument {
                    out.push_str(&format!("= {} ", arg));
                }
                out
            }
            LPCommand::With(properties) => {
                let mut out = String::new();
                out.push_str("\\with { ");
                for p in properties {
                    out.push_str(&p.stringify());
                }
                out.push_str("} ");
                out
            }
            LPCommand::Clef(kind) => format!("\\clef {} ", kind),
            LPCommand::Other(command) => {
                format!("{} ", command)
            }
            LPCommand::Tempo(lp_tempo) => lp_tempo.stringify(),
            LPCommand::Override(property) => {
                let mut out = String::new();
                out.push_str("\\override ");
                out.push_str(&property.stringify());
                out.push_str(" ");
                out
            }
        }
    }
}

/// https://lilypond.org/doc/v2.23/Documentation/notation-big-page.html#repeats
/// Volta and Unfold and Percent repeats are collections
/// Tremolo can take one
///
#[derive(Clone, Debug)]
pub enum LPRepeat {
    Volta,
    Unfold,
    // \repeat percent number musicexpr
    Percent,
    // "The \repeat tremolo syntax expects exactly two notes within
    // the braces, and the number of repetitions must correspond to a
    // note value that can be expressed with plain or dotted notes."
    // [...] "There are two ways to put tremolo marks on a single
    // note. The \repeat tremolo syntax is also used here, in which
    // case the note should not be surrounded by braces"
    Tremolo,
}

// All note head styles: https://lilypond.org/doc/v2.19/Documentation/notation/note-head-styles
#[derive(Clone, Copy, Debug)]
pub enum LPNoteHead {
    Default,
    Cross,
    Harmonic,
    CrossCircle,
    Triangle,
    Slash,
}
impl LPNoteHead {
    fn to_change_command(&self) -> String {
        if !matches!(self, LPNoteHead::Default) {
            let mut s = String::from("\\override NoteHead.style = ");
            s.push_str(match self {
                LPNoteHead::Default => "#'default",
                LPNoteHead::Cross => "#'cross",
                LPNoteHead::Harmonic => "#'harmonic",
                LPNoteHead::CrossCircle => "#'xcircle",
                LPNoteHead::Triangle => "#'triangle",
                LPNoteHead::Slash => "#'slash",
            });
            s.push(' ');
            s
        } else {
            String::new()
        }
    }
}

/// Absolute dynamic marks are specified using a command after a note, such as c4\ff. The available dynamic marks are \ppppp, \pppp, \ppp, \pp, \p, \mp, \mf, \f, \ff, \fff, \ffff, \fffff, \fp, \sf, \sff, \sp, \spp, \sfz, \rfz, and \n
#[derive(Clone, Copy, Debug)]
pub enum LPDynamicKind {
    PPPPP,
    PPPP,
    PPP,
    PP,
    P,
    MP,
    MF,
    F,
    FF,
    FFF,
    FFFF,
    FFFFF,
    FP,
    SF,
    SFF,
    SP,
    Sfz,
    Rfz,
    N,
    Cresc,
    Dim,
    CrescHairpin,
    DimHairpin,
    EndDynamic,
}

impl LPDynamicKind {
    pub fn as_str(&self) -> &'static str {
        match self {
            LPDynamicKind::PPPPP => "ppppp",
            LPDynamicKind::PPPP => "pppp",
            LPDynamicKind::PPP => "ppp",
            LPDynamicKind::PP => "pp",
            LPDynamicKind::P => "p",
            LPDynamicKind::MP => "mp",
            LPDynamicKind::MF => "mf",
            LPDynamicKind::F => "f",
            LPDynamicKind::FF => "ff",
            LPDynamicKind::FFF => "fff",
            LPDynamicKind::FFFF => "ffff",
            LPDynamicKind::FFFFF => "fffff",
            LPDynamicKind::FP => "fp",
            LPDynamicKind::SF => "sf",
            LPDynamicKind::SFF => "sff",
            LPDynamicKind::SP => "sp",
            LPDynamicKind::Sfz => "sfz",
            LPDynamicKind::Rfz => "rfz",
            LPDynamicKind::N => "n",
            LPDynamicKind::Cresc => "cresc",
            LPDynamicKind::Dim => "dim",
            LPDynamicKind::CrescHairpin => "<",
            LPDynamicKind::DimHairpin => ">",
            LPDynamicKind::EndDynamic => "!",
        }
    }
}

#[derive(Clone, Debug)]
pub struct LPDynamic {
    pub dynamic_kind: LPDynamicKind,
    pub direction: LPDirection,
}

impl LPDynamic {
    pub fn stringify(&self) -> String {
        let mut s = String::new();
        s.push(self.direction.to_char());
        s.push('\\');
        s.push_str(self.dynamic_kind.as_str());
        s
    }
}

#[derive(Clone, Debug)]
pub enum LPNodeType {
    Note {
        note_head: LPNoteHead,
    },
    Barline,
    Nil,
    Collection {
        collection_type: LPCollectionType,
        collection: Vec<LPNode>,
    },
    Command(LPCommand),
    TimeSignature(TimeSignature),
    Rest,
    SpacerRest,
    Markup(LPMarkup),
}
impl fmt::Display for LPNodeType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let t = match &self {
            LPNodeType::Note { note_head } => "Note".to_owned(),
            LPNodeType::Barline => "Barline".to_owned(),
            LPNodeType::Nil => "Nil".to_owned(),
            LPNodeType::Collection {
                collection_type,
                collection,
            } => format!("Collection::{}", collection_type),
            LPNodeType::Command(_) => "Command".to_owned(),
            LPNodeType::TimeSignature(_) => "TimeSignature".to_owned(),
            LPNodeType::Rest => "Rest".to_owned(),
            LPNodeType::SpacerRest => "SpacerRest".to_owned(),
            LPNodeType::Markup(_) => "Markup".to_owned(),
        };
        write!(f, "LPNodeType::{}", t)
    }
}

#[derive(Clone, Debug)]
pub enum LPCollectionType {
    Tuplet { n: u32, d: u32 },
    Chord,
    SequentialExpression,
    ParallelExpression,
    GraceNotes,
    Repeat { kind: LPRepeat, number: u32 },
}
impl fmt::Display for LPCollectionType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let t = match &self {
            LPCollectionType::Tuplet { n, d } => "Tuplet",
            LPCollectionType::Chord => "Chord",
            LPCollectionType::SequentialExpression => "SequentialExpression",
            LPCollectionType::ParallelExpression => "ParallelExpression",
            LPCollectionType::GraceNotes => "GraceNotes",
            LPCollectionType::Repeat { kind, number } => "Repeat",
        };
        write!(f, "LPNodeType::{}", t)
    }
}

/// https://lilypond.org/doc/v2.19/Documentation/notation/direction-and-placement
#[derive(Clone, Debug)]
pub enum LPDirection {
    Default,
    Up,
    Down,
}

impl LPDirection {
    pub fn to_char(&self) -> char {
        match &self {
            LPDirection::Default => '-',
            LPDirection::Up => '^',
            LPDirection::Down => '_',
        }
    }
}

/// Lilypond markup is quite comprehensive and complex:
/// https://lilypond.org/doc/v2.19/Documentation/notation/text-markup-commands
/// Only a tiny subset of markup is supported here
#[derive(Clone, Debug)]
pub struct LPMarkup {
    markup_nodes: Vec<LPMarkupNode>,
    pub direction: LPDirection,
}

impl Default for LPMarkup {
    fn default() -> Self {
        Self::new(vec![], LPDirection::Default)
    }
}

impl LPMarkup {
    pub fn new(nodes: Vec<LPMarkupNode>, direction: LPDirection) -> Self {
        Self {
            markup_nodes: nodes,
            direction,
        }
    }
    pub fn push(&mut self, node: LPMarkupNode) {
        self.markup_nodes.push(node);
    }
    pub fn stringify(&self) -> String {
        let mut s = String::new();
        // Don't print anything if there are no nodes
        if !self.markup_nodes.is_empty() {
            s.push(self.direction.to_char());

            s.push_str("\\markup { ");
            for n in &self.markup_nodes {
                s.push_str(&n.stringify());
            }
            s.push('}');
        }
        s
    }
}

/// Small eubset of all the nodes/commands that are available
#[derive(Clone, Debug)]
pub enum LPMarkupNode {
    Text(String),
    Italic,
    Bold,
    Large,
    Dynamic,
}

impl LPMarkupNode {
    pub fn text(s: String) -> Self {
        // Double quotes in the lilypond need to be escaped since they
        // define the boundary of text.
        LPMarkupNode::Text(s.escape_default().to_string())
    }
    pub fn stringify(&self) -> String {
        match &self {
            LPMarkupNode::Text(s) => format!("\"{}\"", s),
            LPMarkupNode::Italic => "\\italic".to_owned(),
            LPMarkupNode::Bold => "\\bold".to_owned(),

            LPMarkupNode::Large => "\\large".to_owned(),
            LPMarkupNode::Dynamic => "\\dynamic".to_owned(),
        }
    }
}

pub enum SpecialToken {
    Command(String),
    BraceBegin,
    BraceEnd,
    TupletRatio(u32, u32),
    ChordBegin,
    ChordEnd {
        tied: bool,
        duration: Fraction,
        dynamics: String,
        articulation: String,
    },
}

/// LPNode is a representation of a Lilypond node within a Lilypond
/// expression. Since a Lilypond score is just one "music expression"
/// containing any number of other nested expressions, some of the
/// `LPNodeType`s are collections containing their own nested "music
/// expression".
///
/// The octave is relative to c (Scientific notation: C4, MIDI note
/// number 48), essentially the number of ' or , octave designations
/// in the Lilypond output.
///
/// This currently only supports a subset of Lilypond and is more
/// useful for generating Lilypond than it is for parsing complex
/// Lilypond. That said, any Lilyond it produces it can also parse.
#[derive(Clone, Debug)]
pub struct LPNode {
    pub node_type: LPNodeType,
    pub note_name: String,
    pub octave: i8,
    pub midi_note: f32,
    pub duration: Fraction,
    pub position_in_piece: PositionInPiece,
    pub articulation: String, // todo: turn into a set
    pub dynamics: Vec<LPDynamic>,
    pub tied: bool, // whether this note is tied to the next (note: not slured as slures are articulation wherease ties are note length)
    pub markups: Vec<LPMarkup>,
    pub commands: Vec<LPCommand>, // Some commads can be attached to notes such as "\startTextSpan"
}

impl Default for LPNode {
    fn default() -> Self {
        LPNode {
            node_type: LPNodeType::Nil,
            note_name: String::new(),
            octave: 0,
            midi_note: 0.,
            duration: Fraction::zero(),
            position_in_piece: PositionInPiece::new(),
            articulation: String::new(),
            dynamics: vec![],
            tied: false,
            markups: vec![],
            commands: vec![],
        }
    }
}

impl LPNode {
    pub fn note_from_midi_note(midi_note: f32, duration: Fraction) -> Self {
        // Round the midi_note value to the nearest integer for
        // generating the note name.  TODO: If microtonal note names
        // are supported, the rounding will have to be changed.
        let (note_name, octave) = midi_note_to_name(midi_note.round());

        LPNode {
            node_type: LPNodeType::Note {
                note_head: LPNoteHead::Default,
            },
            note_name,
            octave,
            midi_note,
            duration,
            ..Default::default()
        }
    }

    pub fn rest(duration: Fraction) -> Self {
        LPNode {
            node_type: LPNodeType::Rest,
            note_name: String::from("r"),
            duration,
            ..Default::default()
        }
    }

    pub fn spacer_rest(duration: Fraction) -> Self {
        LPNode {
            node_type: LPNodeType::SpacerRest,
            note_name: String::from("s"),
            duration,

            ..Default::default()
        }
    }

    pub fn nil() -> Self {
        return LPNode {
            node_type: LPNodeType::Nil,
            note_name: String::from("|"),
            ..Default::default()
        };
    }

    pub fn barline() -> Self {
        return LPNode {
            node_type: LPNodeType::Barline,
            note_name: String::from("|"),
            ..Default::default()
        };
    }

    pub fn tuplet(n: u32, d: u32) -> Self {
        let collection_type = LPCollectionType::Tuplet { n, d };
        return LPNode {
            node_type: LPNodeType::Collection {
                collection_type,
                collection: Vec::new(),
            },
            ..Default::default()
        };
    }

    pub fn sequence() -> Self {
        Self::sequence_from(vec![])
    }

    pub fn sequence_from(collection: Vec<LPNode>) -> Self {
        let collection_type = LPCollectionType::SequentialExpression;
        return LPNode {
            node_type: LPNodeType::Collection {
                collection_type,
                collection,
            },
            ..Default::default()
        };
    }

    pub fn parallel() -> Self {
        Self::parallel_from(vec![])
    }

    pub fn parallel_from(collection: Vec<LPNode>) -> Self {
        let collection_type = LPCollectionType::ParallelExpression;
        return LPNode {
            node_type: LPNodeType::Collection {
                collection_type,
                collection,
            },
            ..Default::default()
        };
    }

    pub fn new_cmd<T: AsRef<str>>(kind: T, argument: Option<T>) -> Self {
        let argument = if let Some(arg) = argument {
            let s = arg.as_ref().to_owned();
            Some(enclose_in_quotation(s))
        } else {
            None
        };
        return LPNode {
            node_type: LPNodeType::Command(LPCommand::New {
                kind: kind.as_ref().to_owned(),
                argument,
            }),
            ..Default::default()
        };
    }

    pub fn with_cmd(properties: Vec<LPProperty>) -> Self {
        return LPNode {
            node_type: LPNodeType::Command(LPCommand::With(properties)),
            ..Default::default()
        };
    }

    pub fn command(command: LPCommand) -> Self {
        return LPNode {
            node_type: LPNodeType::Command(command),
            ..Default::default()
        };
    }
    pub fn override_cmd<T: AsRef<str>>(property_name: T, argument: T) -> Self {
        let prop = LPProperty {
            property: property_name.as_ref().to_owned(),
            argument: argument.as_ref().to_owned(),
        };
        LPNode {
            node_type: LPNodeType::Command(LPCommand::Override(prop)),
            ..Default::default()
        }
    }

    pub fn tempo_metronome_mark(note_value: u32, bpm: u32) -> Self {
        return LPNode {
            node_type: LPNodeType::Command(LPCommand::Tempo(LPTempo {
                text: None,
                markup_object: None,
                metronome_mark: Some(LPMetronomeMark::Fixed { note_value, bpm }),
            })),
            ..Default::default()
        };
    }

    pub fn tempo_metronome_text<T: AsRef<str>>(text: T) -> Self {
        return LPNode {
            node_type: LPNodeType::Command(LPCommand::Tempo(LPTempo {
                text: Some(text.as_ref().to_owned()),
                markup_object: None,
                metronome_mark: None,
            })),
            ..Default::default()
        };
    }

    pub fn tempo(
        metronome_mark: Option<LPMetronomeMark>,
        text: Option<String>,
        markup_object: Option<LPMarkupObject>,
    ) -> Self {
        return LPNode {
            node_type: LPNodeType::Command(LPCommand::Tempo(LPTempo {
                text,
                markup_object,
                metronome_mark,
            })),
            ..Default::default()
        };
    }

    pub fn clef<T: AsRef<str>>(kind: T) -> Self {
        return LPNode {
            node_type: LPNodeType::Command(LPCommand::Clef(kind.as_ref().to_owned())),
            ..Default::default()
        };
    }

    pub fn grace_notes() -> Self {
        let collection_type = LPCollectionType::GraceNotes;
        return LPNode {
            node_type: LPNodeType::Collection {
                collection_type,
                collection: Vec::new(),
            },
            ..Default::default()
        };
    }

    pub fn chord() -> Self {
        let collection_type = LPCollectionType::Chord;
        return LPNode {
            node_type: LPNodeType::Collection {
                collection_type,
                collection: Vec::new(),
            },
            ..Default::default()
        };
    }

    pub fn other_cmd<T: AsRef<str>>(s: T) -> Self {
        return LPNode {
            node_type: LPNodeType::Command(LPCommand::Other(s.as_ref().to_owned())),
            ..Default::default()
        };
    }

    pub fn time_sig(numerator: u32, denominator: u32) -> Self {
        return LPNode {
            node_type: LPNodeType::TimeSignature(TimeSignature::from(numerator, denominator)),
            ..Default::default()
        };
    }

    pub fn tremolo_single(node: LPNode, resulting_duration: Fraction) -> Self {
        let num_repeats: u32 = (resulting_duration / node.duration).to_u32().unwrap();
        let collection_type = LPCollectionType::Repeat {
            kind: LPRepeat::Tremolo,
            number: num_repeats,
        };
        let collection = vec![node];
        return LPNode {
            node_type: LPNodeType::Collection {
                collection_type,
                collection,
            },
            ..Default::default()
        };
    }
    pub fn tremolo_double(node1: LPNode, node2: LPNode, resulting_duration: Fraction) -> Self {
        if node1.duration != node2.duration {
            eprintln!("ERROR: Two notes in double tremolo with different durations");
        }
        let note_values = node1.duration + node2.duration;
        let num_repeats: u32 = (resulting_duration / note_values).to_u32().unwrap();
        let collection_type = LPCollectionType::Repeat {
            kind: LPRepeat::Tremolo,
            number: num_repeats,
        };
        let collection = vec![node1, node2];
        return LPNode {
            node_type: LPNodeType::Collection {
                collection_type,
                collection,
            },
            ..Default::default()
        };
    }

    /// TODO: Tremolo can have max 2 nodes
    pub fn push(&mut self, node: LPNode) {
        if let LPNodeType::Collection {
            collection_type: _,
            ref mut collection,
        } = self.node_type
        {
            collection.push(node);
        }
        self.calculate_length();
    }
    pub fn push_all(&mut self, nodes: Vec<LPNode>) {
        if let LPNodeType::Collection {
            collection_type: _,
            ref mut collection,
        } = self.node_type
        {
            collection.extend(nodes.into_iter());
        }
        self.calculate_length();
    }
    pub fn push_command(&mut self, command: LPCommand) {
        self.commands.push(command);
    }

    pub fn push_markup(&mut self, markup: LPMarkup) {
        self.markups.push(markup);
    }
    pub fn set_markup(&mut self, index: usize, markup: LPMarkup) {
        self.markups[index] = markup;
    }

    pub fn push_dynamic(&mut self, dynamic: LPDynamic) {
        // If the node is a collection, the dynamic should be put on the first node of the collection
        match &mut self.node_type {
            LPNodeType::Note { note_head: _ } | LPNodeType::Rest | LPNodeType::SpacerRest => {
                self.dynamics.push(dynamic);
            }
            LPNodeType::Barline => {}
            LPNodeType::Nil => {}
            LPNodeType::Collection {
                collection_type: _,
                collection,
            } => {
                if !collection.is_empty() {
                    collection[0].push_dynamic(dynamic);
                }
            }
            LPNodeType::Command(_) => {}
            LPNodeType::TimeSignature(_) => {}
            LPNodeType::Markup(_) => {}
        }
    }
    /// Pushes a markup node to the last markup object
    pub fn push_markup_node(&mut self, markup_node: LPMarkupNode) {
        if let Some(markup) = self.markups.last_mut() {
            markup.push(markup_node);
        }
    }

    /// Used to calculate the duration of LPNodes which are
    /// collections or otherwise need their durations calculated
    /// separately from the durational value of the individual note.
    pub fn calculate_length(&mut self) {
        // if this is a collection, calculate the length of the collection
        let mut length = Fraction::zero();
        match &mut self.node_type {
            LPNodeType::Collection {
                collection_type,
                collection,
            } => {
                match collection_type {
                    LPCollectionType::Tuplet { n, d } => {
                        // add all the notes in the collection together
                        for node in collection {
                            node.calculate_length();
                            length += node.duration;
                        }
                        // apply the tuplet transformation
                        length *= Fraction::new(*d, *n);
                    }
                    LPCollectionType::Chord => {
                        length = self.duration;
                    }
                    LPCollectionType::SequentialExpression => {
                        // The duration is the total length of the expressions therein
                        for node in collection {
                            node.calculate_length();
                            length += node.duration;
                        }
                    }
                    LPCollectionType::ParallelExpression => {
                        // The duration is the longest subduration of the expression
                        for node in collection {
                            node.calculate_length();
                            if node.duration > length {
                                length = node.duration;
                            }
                        }
                    }
                    LPCollectionType::GraceNotes => {
                        // The duration of grace notes is 0
                        length = Fraction::zero();
                    }
                    LPCollectionType::Repeat { kind, number } => {
                        let mut total_collection_length = Fraction::zero();
                        for node in collection {
                            node.calculate_length();
                            total_collection_length += node.duration;
                        }
                        match kind {
                            LPRepeat::Volta => {
                                // The nodes contained show up only once in the
                                // score, with repeat barlines but the music
                                // will have twice the duration when
                                // played. Align for score duration or actual
                                // duration?
                                // Since the primary use of this function for now is to
                                // split notes that are crossing a bar line it makes
                                // more sense to go with the duration that is visible
                                // in the score.
                                length = total_collection_length;
                            }
                            LPRepeat::Unfold | LPRepeat::Percent | LPRepeat::Tremolo => {
                                length = total_collection_length * Fraction::from(*number as u64);
                            }
                        }
                    }
                }
            }
            LPNodeType::Note { .. } | LPNodeType::Rest | LPNodeType::SpacerRest => {
                length = self.duration;
            }
            LPNodeType::Barline
            | LPNodeType::Nil
            | LPNodeType::Command(_)
            | LPNodeType::TimeSignature(_)
            | LPNodeType::Markup(_) => (),
        }
        self.duration = length;
    }

    pub fn get_collection_mut(&mut self) -> Option<&mut Vec<LPNode>> {
        match &mut self.node_type {
            LPNodeType::Collection {
                collection_type: _,
                collection,
            } => Some(collection),
            _ => None,
        }
    }

    pub fn set_collection(&mut self, new_nodes: Vec<LPNode>) {
        match &mut self.node_type {
            LPNodeType::Collection {
                collection_type: _,
                ref mut collection,
            } => {
                *collection = new_nodes;
            }
            _ => (),
        }
    }

    pub fn set_note_head(&mut self, new_note_head: LPNoteHead) {
        if let LPNodeType::Note { note_head } = &mut self.node_type {
            *note_head = new_note_head;
        } else {
            eprintln!("Cannot set note head of non note LPNodeType");
        }
    }

    pub fn split_compound_durations(&mut self, time_signatures: &[TimeSignature]) {
        if let Some(nodes) = self.get_collection_mut() {
            *nodes = split_compound_duration_notes(nodes, time_signatures);
        }
    }

    pub fn calculate_bar_positions(
        &mut self,
        time_signatures: &[TimeSignature],
        offset: PositionInPiece,
    ) {
        let mut position_in_piece = offset;
        calculate_bar_positions(
            self,
            &mut position_in_piece,
            time_signatures,
            Fraction::from(1u64),
        );
    }

    pub fn stringify_duration(&self) -> String {
        let mut s = String::new();
        if self.duration.numer() == Some(&1u64) {
            s = self.duration.denom().unwrap().to_string();
        } else if self.duration.numer() == Some(&3u64) {
            s = (self.duration.denom().unwrap() / 2).to_string();
            s.push('.');
        } else if self.duration.numer() == Some(&7u64) {
            s = (self.duration.denom().unwrap() / 4).to_string();
            s.push_str("..");
        } else if self.duration.numer() == Some(&0u64) {
            s = String::new();
        } else {
            // only numerators of 1 or 3 can be written as notes, otherwise they have to be split into tied notes
            println!(
                "% length {}/{} cannot be parsed into lilypond",
                self.duration.numer().unwrap(),
                self.duration.denom().unwrap()
            );
        }
        s
    }

    pub fn stringify_note(&self) -> String {
        // produce the right octave string based on the octave number (German system)
        // e.g. 2 -> '' , -3 -> ,,,
        let mut octave_string = String::new();
        let mut octave_sign = "";
        if self.octave > 0 {
            octave_sign = "'";
        } else if self.octave < 0 {
            octave_sign = ",";
        }
        for _ in 0..self.octave.abs() {
            octave_string.push_str(octave_sign);
        }
        let mut end_characters = String::new(); // For commands following the note and the like
                                                // create the string representation of the note
        let mut note_string = match self.node_type {
            LPNodeType::Note { note_head } => {
                // TODO: Have a generator state with the current note head so that it only inserts commands if the note head style has changed
                let note_head_change = note_head.to_change_command();
                if note_head_change.len() != 0 {
                    end_characters.push_str(" \\revert NoteHead.style");
                }
                format!(
                    "{}{}{}{}",
                    note_head_change,
                    self.note_name,
                    octave_string,
                    self.stringify_duration()
                )
            }
            LPNodeType::Rest => format!("r{}", self.stringify_duration()),
            LPNodeType::SpacerRest => format!("s{}", self.stringify_duration()),
            _ => String::new(),
        };
        if !self.dynamics.is_empty() {
            for d in &self.dynamics {
                note_string.push_str(&d.stringify());
            }
        }
        if !self.articulation.is_empty() {
            note_string.push_str(&self.articulation.to_string());
        }
        // Rests cannot be tied over
        if matches!(self.node_type, LPNodeType::Note { .. }) {
            if self.tied {
                note_string.push('~');
            }
        }
        for m in &self.markups {
            note_string.push_str(&m.stringify());
        }
        for c in &self.commands {
            println!("attached command: {}", c.stringify());
            note_string.push_str(&c.stringify());
        }

        note_string.push_str(end_characters.as_str()); // Add potential following commands
        note_string
    }

    pub fn stringify(&self) -> String {
        match &self.node_type {
            LPNodeType::Nil => String::from(""),
            LPNodeType::Barline => String::from("|\n"),
            LPNodeType::Note { .. } => self.stringify_note(),
            LPNodeType::Collection {
                collection_type,
                collection,
            } => {
                let mut string = String::from("");
                let mut end_characters;
                match collection_type {
                    LPCollectionType::Tuplet { d, n } => {
                        string.push_str(&format!("\\tuplet {}/{} {{ ", n, d));
                        end_characters = String::from("}");
                    }
                    LPCollectionType::Chord => {
                        string.push('<');
                        end_characters = String::from(">");
                        end_characters.push_str(&self.stringify_duration()); // the chord length comes after the chord
                        if !self.dynamics.is_empty() {
                            for d in &self.dynamics {
                                end_characters.push_str(&d.stringify());
                            }
                        }
                        if !self.articulation.is_empty() {
                            end_characters.push_str(&self.articulation.to_string());
                        }
                        if self.tied {
                            end_characters.push('~');
                        }
                        for m in &self.markups {
                            end_characters.push_str(&m.stringify());
                        }
                    }
                    LPCollectionType::SequentialExpression => {
                        string.push_str("{ ");
                        end_characters = String::from("}");
                    }
                    LPCollectionType::ParallelExpression => {
                        string.push_str("<< ");
                        end_characters = String::from(">>");
                    }
                    LPCollectionType::GraceNotes => {
                        string.push_str("\\grace { ");
                        end_characters = String::from("}");
                    }
                    LPCollectionType::Repeat { kind, number } => {
                        string.push_str("\\repeat ");
                        match kind {
                            LPRepeat::Volta => {
                                string.push_str(&format!("volta {} {{", number));
                                end_characters = String::from("}");
                            }
                            LPRepeat::Unfold => {
                                string.push_str(&format!("unfold {} {{", number));
                                end_characters = String::from("}");
                            }
                            LPRepeat::Percent => {
                                string.push_str(&format!("percent {} {{", number));
                                end_characters = String::from("}");
                            }
                            LPRepeat::Tremolo => {
                                if collection.len() == 1 {
                                    // For tremolo with a single note, the braces can be omitted
                                    string.push_str(&format!("tremolo {} ", number));
                                    end_characters = String::new();
                                } else if collection.len() == 2 {
                                    end_characters = String::from("}");
                                    string.push_str(&format!("tremolo {} {{ ", number));
                                } else {
                                    eprintln!("Trying to stringify tremolo with more than 2 notes. This is not valid lilypond.");
                                    end_characters = String::from("}");
                                }
                            }
                        }
                    }
                }
                for c in &self.commands {
                    end_characters.push_str(&c.stringify());
                }
                for n in collection {
                    string.push_str(&format!("{} ", n.stringify()));
                }
                string.push_str(&end_characters);
                string.push('\n');
                string
            }
            LPNodeType::Command(lp_command) => lp_command.stringify(),
            LPNodeType::TimeSignature(time_signature) => String::from(&format!(
                "\\time {}/{}",
                time_signature.numerator, time_signature.denominator
            )),
            LPNodeType::Rest | LPNodeType::SpacerRest => self.stringify_note(),
            LPNodeType::Markup(lp_markup) => lp_markup.stringify(),
        }
    }
}

impl fmt::Display for LPNode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "LPNode: {}, dur: {}", self.node_type, self.duration)
    }
}
/// Calculates the position of every node in the piece and also
/// inserts barlines where appropriete to match time signatures.
/// TODO: Does not yet respect lilypond time signature settings
fn calculate_bar_positions(
    node: &mut LPNode,
    position_in_piece: &mut PositionInPiece,
    time_signatures: &[TimeSignature],
    // the length_mult is for tuplets and other constructs which change the durational value of a length
    length_mult: Fraction,
) {
    // give this node the current position
    node.position_in_piece = position_in_piece.clone();
    match &mut node.node_type {
        LPNodeType::Collection {
            collection_type,
            ref mut collection,
        } => {
            // This node already has a duration that will be added, but the
            // elements of the collection have to be placed one after the other.
            let mut collection_position = position_in_piece.clone();
            match collection_type {
                LPCollectionType::Tuplet { n, d } => {
                    // recursively go through the tuplets
                    for loc_node in collection {
                        calculate_bar_positions(
                            loc_node,
                            &mut collection_position,
                            time_signatures,
                            length_mult * Fraction::new(*d, *n),
                        );
                    }
                }
                LPCollectionType::Chord => {
                    for node in collection {
                        node.position_in_piece = position_in_piece.clone();
                    }
                }
                LPCollectionType::SequentialExpression => {
                    // Call calculate_bar_positions on the collection, as it is just a sequence of nodes
                    for loc_node in collection {
                        calculate_bar_positions(
                            loc_node,
                            &mut collection_position,
                            time_signatures,
                            length_mult,
                        );
                    }
                }
                LPCollectionType::ParallelExpression => {
                    // Each node should be called in parallel, forking the current settings
                    for loc_node in collection {
                        calculate_bar_positions(
                            loc_node,
                            &mut position_in_piece.clone(),
                            time_signatures,
                            length_mult,
                        );
                    }
                }
                LPCollectionType::GraceNotes => {
                    // A grace note has no duration in a music
                    // notation sense so give them all the current
                    // duration and don't increment it. GraceNotes
                    // have 0 duration so this_node_length below
                    // should be 0.
                    for node in collection {
                        node.position_in_piece = position_in_piece.clone();
                    }
                }
                LPCollectionType::Repeat { kind: _, number: _ } => {
                    // In a repeat, except for the volta, every instace of the nodes contained have multiple positions.
                    // We will ignore this for now.
                    for local_node in collection {
                        calculate_bar_positions(
                            local_node,
                            &mut collection_position,
                            time_signatures,
                            length_mult,
                        );
                    }
                }
            }
        }
        _ => (),
    }
    // Calculate the PositionInPiece for the next node:
    // add the length of this note to the bar counter whatever the node type
    // Types with no duration should have that duration calculated while parsing
    // or recursively before running this function.
    let this_node_length = node.duration * Fraction::from(length_mult);
    position_in_piece.bar_position += this_node_length;
    position_in_piece.raw_position += this_node_length;
    if position_in_piece.bar_number as usize >= time_signatures.len() {
        eprintln!(
            "Out of bounds ({}/{}): {}, position: {}",
            position_in_piece.bar_number,
            time_signatures.len(),
            node,
            position_in_piece.raw_position.to_f32().unwrap(),
        );
    } else {
        let mut current_time_signature = &time_signatures[position_in_piece.bar_number as usize];
        while position_in_piece.bar_position >= current_time_signature.to_fraction() {
            position_in_piece.bar_number += 1;
            position_in_piece.bar_position -= current_time_signature.to_fraction();
            if position_in_piece.bar_position == Fraction::zero() {
                // We definitely don't need to increment the bar counter and we
                // may have hit the end of the piece in which case there are no
                // more time signatures.
                break;
            }
            if position_in_piece.bar_number as usize >= time_signatures.len() {
                eprintln!(
                    "Out of bounds (inside) ({}/{}): {}, position: {}",
                    position_in_piece.bar_number,
                    time_signatures.len(),
                    node,
                    position_in_piece.raw_position.to_f32().unwrap(),
                );
            } else {
                current_time_signature = &time_signatures[position_in_piece.bar_number as usize];
            }
        }
    }
}

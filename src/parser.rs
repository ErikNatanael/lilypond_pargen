use fraction::Fraction;
use fraction::Zero;

use crate::lp_node::*;

pub struct Parser<'a> {
    parsing_state: ParsingState,
    index: usize,
    tokens: Vec<&'a str>,
}

impl<'a> Parser<'a> {
    pub fn parse_lilypond_from_string<T: AsRef<str>>(data: T) -> LPDocument {
        let mut parser = Parser {
            parsing_state: ParsingState::new(),
            index: 0,
            // Split the data into tokens that can be parsed one by one
            tokens: data.as_ref().split_whitespace().collect(),
        };

        // TODO: Implement header, and midi here
        // For now, assume we're thrown straight into the \score part
        let mut nodes: Vec<LPNode> = vec![];
        while parser.index < parser.tokens.len() {
            let result = parser.start_parse_token();
            if let (Some(node), None) = result {
                nodes.push(node);
            }
        }
        LPDocument::from_score(if nodes.len() == 1 {
            nodes.remove(0) // returns the element at 0 from the Vec
        } else {
            // Wrap the collection in a sequential collection
            LPNode::sequence_from(nodes)
        })
    }

    /// Start the parsing process for a token. This will first try to
    /// parse the token as a single token into an LPNode. If it
    /// notices that the current token is the start of some command or
    /// collection involving several tokens, it will call the
    /// appropriate function for that e.g. `parse_tuplet`,
    /// `parse_chord` etc.
    fn start_parse_token(&mut self) -> (Option<LPNode>, Option<SpecialToken>) {
        let token = self.tokens[self.index];
        self.index += 1;
        let res = Parser::parse_single_token(token, &mut self.parsing_state);
        match res {
            (None, Some(SpecialToken::Command(s))) => {
                match s.as_str() {
                    "tuplet" => {
                        // index has already been incremented above
                        let tuplet_res = self.parse_tuplet();
                        match tuplet_res {
                            Ok(tuplet) => {
                                return (Some(tuplet), None);
                            }
                            Err(e) => {
                                panic!(e);
                            } // perhaps just print the value and return a (None, None) instead?
                        }
                    }
                    "grace" => {
                        // index has already been incremented above
                        let grace_res = self.parse_grace_notes();
                        match grace_res {
                            Ok(grace_notes) => {
                                return (Some(grace_notes), None);
                            }
                            Err(e) => {
                                panic!(e);
                            } // perhaps just print the value and return a (None, None) instead?
                        }
                    }
                    &_ => return (Some(LPNode::other_cmd(s)), None),
                }
            }
            (None, Some(SpecialToken::ChordBegin)) => {
                // index has already been incremented above
                let chord_res = self.parse_chord();
                match chord_res {
                    Ok(chord) => {
                        return (Some(chord), None);
                    }
                    Err(e) => {
                        panic!(e);
                    } // perhaps just print the value and return a (None, None) instead?
                }
            }
            (None, Some(SpecialToken::BraceBegin)) => {
                // index has already been incremented above
                // This is the start of a sequential music expression
                let seq_res = self.parse_sequential_expression();
                match seq_res {
                    Ok(seq) => {
                        return (Some(seq), None);
                    }
                    Err(e) => {
                        panic!(e);
                    } // perhaps just print the value and return a (None, None) instead?
                }
            }
            _ => {
                return res;
            }
        }
    }

    /// Often a single token can be parsed to an LPNode, but sometimes
    /// it can't. This function tries to parse the token, but may
    /// return a `SpecialToken` if the token indicates the start of a
    /// command, chord, music expression etc. In that case, the
    /// tokens following it will need to be given special treatment.
    fn parse_single_token(
        s: &str,
        state: &mut ParsingState,
    ) -> (Option<LPNode>, Option<SpecialToken>) {
        let mut parse_mode = NoteParseMode::Note;
        let mut is_chord = false; // the end of chords is very similar to notes so parse them the same way
        let mut is_rest = false;
        if let Some(SpecialToken::TupletRatio(_, _)) = state.expected_token {
            // parse as if this token is the tuplet ratio
            // syntax: n/n e.g. 5/3 or 15/16
            let mut denom_string = String::from("");
            let mut nom_string = String::from("");
            let mut at_nom = false;
            for (i, c) in s.chars().enumerate() {
                if c == '/' {
                    at_nom = true;
                } else if c.is_digit(10) {
                    if !at_nom {
                        denom_string.push(c);
                    } else {
                        nom_string.push(c);
                    }
                }
            }
            if !denom_string.is_empty() && !nom_string.is_empty() {
                return (
                    None,
                    Some(SpecialToken::TupletRatio(
                        denom_string.parse().unwrap(),
                        nom_string.parse().unwrap(),
                    )),
                );
            } else {
                println!("tuplet ratio error: {}", s);
                return (None, None);
            }
        }
        if s.is_empty() {
            return (None, None);
        } else if s.as_bytes()[0] == b'|' {
            return (Some(LPNode::barline()), None);
        } else if s.as_bytes()[0] == b'\\' {
            // this is a command, return it without the backslash
            return (
                None,
                Some(SpecialToken::Command(String::from(s).split_off(1))),
            );
        } else if s.as_bytes()[0] == b'{' {
            return (None, Some(SpecialToken::BraceBegin));
        } else if s.as_bytes()[0] == b'}' {
            return (None, Some(SpecialToken::BraceEnd));
        } else if s.as_bytes()[0] == b'<' {
            return (None, Some(SpecialToken::ChordBegin));
        } else if s.as_bytes()[0] == b'>' {
            is_chord = true;
        } else if s.as_bytes()[0] == b'r' {
            is_rest = true;
        }

        // find the first non-alphabetic character to get the note name
        let breakpoint = 0;
        let mut note_name_vec = vec![];
        let mut dynamic_vec = vec![];
        let mut articulation_vec = vec![];
        let mut dur_string = String::new();
        let mut dotted_note = false;
        let mut octave = 0;
        let mut tied_over_note = false;

        for (i, c) in s.chars().enumerate() {
            // do something with character `c` and index `i`
            // check if parse mode should be changed
            if let Some(pm) = check_note_parse_mode(c) {
                parse_mode = pm;
            }
            if c == '~' {
                tied_over_note = true;
                continue; // don't register this symbol as articulation of dynamic
            }
            match parse_mode {
                NoteParseMode::Dynamic => {
                    dynamic_vec.push(c);
                }
                NoteParseMode::Articulation => {
                    articulation_vec.push(c);
                }
                _ => {
                    if c.is_alphabetic() && c != 'r' {
                        note_name_vec.push(c);
                    } else if c.is_digit(10) {
                        // a note length can be several digits
                        dur_string.push(c);
                    } else if c == '.' {
                        dotted_note = true;
                    }
                    // parse octave
                    // an octave can consist of several characters e.g. c'' c,,
                    else if c == '\'' {
                        octave += 1
                    } else if c == ',' {
                        octave -= 1
                    }
                }
            }
        }
        let mut dur = state.current_dur;
        if !dur_string.is_empty() {
            // set global state length to the length registered here
            let mut numerator: u64 = 1;
            let mut denominator: u64 = dur_string.parse().unwrap();
            if dotted_note {
                denominator *= 2;
                numerator = 3;
            }
            dur = Fraction::new(numerator, denominator);
        }

        if is_chord {
            return (
                None,
                Some(SpecialToken::ChordEnd {
                    tied: tied_over_note,
                    duration: dur,
                    articulation: articulation_vec.into_iter().collect(),
                    dynamics: dynamic_vec.into_iter().collect(),
                }),
            );
        } else if is_rest {
            return (
                Some(LPNode {
                    node_type: LPNodeType::Rest,
                    note_name: String::from("r"),
                    duration: dur,
                    position_in_piece: PositionInPiece::new(),
                    articulation: articulation_vec.into_iter().collect(),
                    // dynamic: dynamic_vec.into_iter().collect(),
                    tied: tied_over_note,
                    ..Default::default()
                }),
                None,
            );
        }

        let note_name: String = note_name_vec.into_iter().collect();
        let mut midi_note: f32 = 0.0;
        match note_name_to_midi_note(&note_name, octave) {
            Ok(n) => {
                midi_note = n;
            }
            Err(e) => {
                println!("{}", e);
            }
        }

        state.current_dur = dur.clone();
        (
            Some(LPNode {
                node_type: LPNodeType::Note {
                    note_head: LPNoteHead::Default,
                },
                note_name,
                octave, // absolute octave notation
                midi_note,
                duration: dur,
                articulation: articulation_vec.into_iter().collect(),
                // dynamic: dynamic_vec.into_iter().collect(),
                tied: tied_over_note,
                ..Default::default()
            }),
            None,
        )
    }

    fn parse_tuplet(&mut self) -> Result<LPNode, &'static str> {
        self.parsing_state.expected_token = Some(SpecialToken::TupletRatio(0, 0));
        let res = Parser::parse_single_token(self.tokens[self.index], &mut self.parsing_state);
        let d;
        let n;
        // this should be a tuplet ratio or something is wrong
        if let (None, Some(SpecialToken::TupletRatio(a, b))) = res {
            n = a;
            d = b;
        } else {
            return Err("parse_tuplet: Expected a triplet ratio but found none.");
            // if not something is wrong with the data
        }
        self.parsing_state.expected_token = None;
        // next should be an opening brace
        self.index += 1;
        let res = Parser::parse_single_token(self.tokens[self.index], &mut self.parsing_state);
        if let (None, Some(SpecialToken::BraceBegin)) = res {
            // do nothing
        } else {
            return Err("parse_tuplet: Expected an opening brace, but found none.");
        }
        let mut tuplet = LPNode::tuplet(n, d);
        // expected: notes contained in tuplet
        self.index += 1;
        while self.index < self.tokens.len() {
            let result = self.start_parse_token(); // parse token increments the index after getting 1 token
            match result {
                (Some(node), None) => tuplet.push(node),
                (None, Some(SpecialToken::BraceEnd)) => {
                    // found the ending brace, return the finished tuplet
                    tuplet.calculate_length();
                    return Ok(tuplet);
                }
                _ => (),
            }
        }
        // no ending brace was found before we ran out of tokens, return an error
        return Err("parse_tuplet: No ending brace found, ran out of tokens.");
    }

    fn parse_grace_notes(&mut self) -> Result<LPNode, &'static str> {
        self.parsing_state.expected_token = None;
        // next should be an opening brace
        self.index += 1;
        let res = Parser::parse_single_token(self.tokens[self.index], &mut self.parsing_state);
        if let (None, Some(SpecialToken::BraceBegin)) = res {
            // do nothing
        } else {
            return Err("parse_grace_notes: Expected an opening brace, but found none.");
        }
        let mut grace_notes = LPNode::grace_notes();
        // expected: notes contained in the grace notes expression
        self.index += 1;
        while self.index < self.tokens.len() {
            let result = self.start_parse_token(); // parse token increments the index after getting 1 token
            match result {
                (Some(node), None) => grace_notes.push(node),
                (None, Some(SpecialToken::BraceEnd)) => {
                    // found the ending brace, return the finished grace_notes
                    grace_notes.calculate_length();
                    return Ok(grace_notes);
                }
                _ => (),
            }
        }
        // no ending brace was found before we ran out of tokens, return an error
        return Err("parse_tuplet: No ending brace found, ran out of tokens.");
    }

    fn parse_sequential_expression(&mut self) -> Result<LPNode, &'static str> {
        // A BraceBegin has been found, but no command before it. This
        // means that this is a sequential music expression
        self.parsing_state.expected_token = None;
        let mut seq = LPNode::sequence();
        // expected: notes contained in the expression
        self.index += 1;
        while self.index < self.tokens.len() {
            let result = self.start_parse_token(); // parse token increments the index after getting 1 token
            match result {
                (Some(node), None) => seq.push(node),
                (None, Some(SpecialToken::BraceEnd)) => {
                    // found the ending brace, return the finished grace_notes
                    seq.calculate_length();
                    return Ok(seq);
                }
                _ => (),
            }
        }
        // no ending brace was found before we ran out of tokens, return an error
        return Err("parse_tuplet: No ending brace found, ran out of tokens.");
    }

    fn parse_chord(&mut self) -> Result<LPNode, &'static str> {
        let mut chord = LPNode::chord();
        // expected: notes contained in chord
        // This will hopefully find
        // the end of a chord and return before running out of tokens
        while self.index < self.tokens.len() {
            let result = self.start_parse_token(); // parse token increments the index after getting 1 token
            match result {
                (Some(node), None) => chord.push(node),
                (
                    None,
                    Some(SpecialToken::ChordEnd {
                        tied,
                        duration,
                        dynamics,
                        articulation,
                    }),
                ) => {
                    chord.tied = tied;
                    chord.duration = duration;
                    // chord.dynamic = dynamics;
                    chord.articulation = articulation;
                    let collection = chord.get_collection_mut().unwrap();
                    for node in collection {
                        node.duration = Fraction::zero();
                    }
                    // found the ending tag, return the finished chord
                    return Ok(chord);
                }
                _ => (),
            }
        }
        // no ending brace was found before we ran out of tokens, return an error
        return Err("parse_chord: No ending tag found, ran out of tokens.");
    }
}

// TODO: move parsing from LFNode to parser in order to be able to move ParsingState out from the lp_node module
pub struct ParsingState {
    current_octave: i8, // only used for relative pitch to keep track of the correct octave
    pub current_dur: Fraction,
    tied_over_note: bool,
    pub expected_token: Option<SpecialToken>,
}

impl ParsingState {
    pub fn new() -> Self {
        ParsingState {
            current_octave: 0,
            current_dur: Fraction::new(1u8, 4u8),
            tied_over_note: false,
            expected_token: None,
        }
    }
}

enum NoteParseMode {
    Note,
    Dynamic,
    Articulation,
    ChordEnd,
}

fn check_note_parse_mode(c: char) -> Option<NoteParseMode> {
    if c == '-' {
        return Some(NoteParseMode::Articulation);
    } else if c == '\\' {
        return Some(NoteParseMode::Dynamic);
    }
    return None;
}

/// Splits notes so that they can be written with standard notation, meaning that they can only
/// have a numerator of 1 or 3, 3 being a dotted note value. Notes also cannot cross bar lines.
/// Returns a new Vec of nodes with split durations.
pub fn split_compound_duration_notes(
    nodes: &Vec<LPNode>,
    time_signatures: &[TimeSignature],
) -> Vec<LPNode> {
    let mut res_nodes = vec![];
    // Commands need to come right after the node with the starting position
    // they are associated with. If they instead come after e.g. a split rest
    // they will start later than they should. Therefore, whenever something is
    // split, the second part of it gets stored in after_command_nodes which
    // will be added to the res_nodes when it is checked that the following node
    // is not any kind of command or similar.
    let mut after_command_nodes: Vec<LPNode> = vec![];
    for node in nodes {
        if !matches!(node.node_type, LPNodeType::Command(_)) {
            for n in &after_command_nodes {
                res_nodes.push(n.clone());
            }
            after_command_nodes.clear();
        } else {
            println!("Command: {:?}", node);
        }
        match &node.node_type {
            LPNodeType::Collection {
                collection_type,
                collection,
            } => {
                match collection_type {
                    LPCollectionType::Chord => {
                        let durations = durations_from_compound(
                            &node.duration,
                            time_signatures,
                            &node.position_in_piece,
                        );
                        let num_durs = durations.len();
                        for (i, length) in durations.into_iter().enumerate() {
                            let mut new_node = node.clone();
                            new_node.duration = length;
                            if i != num_durs - 1 {
                                new_node.tied = true;
                            }
                            if i == 0 {
                                res_nodes.push(new_node);
                            } else {
                                after_command_nodes.push(new_node);
                            }
                        }
                    }

                    LPCollectionType::Repeat { kind, number } => {
                        match kind {
                            LPRepeat::Volta => {
                                todo!()
                            }
                            LPRepeat::Unfold => {
                                todo!()
                            }
                            LPRepeat::Percent => {
                                todo!()
                            }
                            LPRepeat::Tremolo => {
                                // Splitting a tremolo is a little bit more
                                // complicated than other structures. It
                                // requires the lengths to be at least 1x the
                                // combine length of the nodes contined within.
                                //
                                // Get durs:
                                let smallest_dur = collection
                                    .iter()
                                    .fold(Fraction::zero(), |sum, n| sum + n.duration);
                                let durations = durations_from_compound(
                                    &node.duration,
                                    time_signatures,
                                    &node.position_in_piece,
                                );
                                for (i, length) in durations.into_iter().enumerate() {
                                    let new_node = match collection.len() {
                                        1 => {
                                            let mut inner_node = collection[0].clone();
                                            if i != 0 {
                                                // If a tremolo is split, don't repeat dynamics, text etc.
                                                inner_node.dynamics = vec![];
                                                inner_node.markups = vec![];
                                            }
                                            if *length.numer().unwrap() == 3 {
                                                // If the length is dotted, the
                                                // note needs to be dotted.
                                                inner_node.duration *= Fraction::new(3_u64, 2_u64);
                                            }
                                            LPNode::tremolo_single(inner_node, length)
                                        }
                                        2 => {
                                            let mut inner_node0 = collection[0].clone();
                                            let mut inner_node1 = collection[1].clone();
                                            if i != 0 {
                                                // If a tremolo is split, don't repeat dynamics, text etc.
                                                inner_node0.dynamics = vec![];
                                                inner_node0.markups = vec![];
                                            }
                                            if *length.numer().unwrap() == 3 {
                                                // If the length is dotted, the
                                                // note needs to be dotted.
                                                inner_node0.duration *= Fraction::new(3u64, 2u64);
                                                inner_node1.duration *= Fraction::new(3u64, 2u64);
                                            }
                                            LPNode::tremolo_double(inner_node0, inner_node1, length)
                                        }
                                        _ => {
                                            eprintln!("ERROR: Trying to split tremolo with {} inner nodes", collection.len());
                                            LPNode::spacer_rest(length)
                                        }
                                    };
                                    // new_node.duration = length;
                                    if i == 0 {
                                        res_nodes.push(new_node);
                                    } else {
                                        after_command_nodes.push(new_node);
                                    }
                                }
                            }
                        }
                    }
                    _ => {
                        // Clone the collection node and split the durations of all of the nodes it contains.
                        let mut new_node = node.clone();
                        let new_collection = split_compound_duration_notes(
                            new_node.get_collection_mut().unwrap(),
                            time_signatures,
                        );
                        new_node.set_collection(new_collection);
                        res_nodes.push(new_node);
                    }
                }
            }
            LPNodeType::Note { .. } | LPNodeType::Rest | LPNodeType::SpacerRest => {
                let durations = durations_from_compound(
                    &node.duration,
                    time_signatures,
                    &node.position_in_piece,
                );
                // println!("lengths are: {:?}", lengths);
                let num_durs = durations.len();
                for (i, length) in durations.into_iter().enumerate() {
                    let mut new_node = node.clone();
                    new_node.duration = length;
                    if matches!(new_node.node_type, LPNodeType::Rest)
                        && length >= Fraction::from(0.5)
                    {
                        new_node.node_type = LPNodeType::SpacerRest;
                    }
                    if i != num_durs - 1 {
                        new_node.tied = true;
                    }
                    if i != 0 {
                        // A tied over note shopuldn't repeat dynamics and text
                        new_node.dynamics = vec![];
                        new_node.markups = vec![];
                        new_node.commands = vec![];
                    }
                    if i == 0 {
                        res_nodes.push(new_node);
                    } else {
                        after_command_nodes.push(new_node);
                    }
                }
            }
            _ => {
                res_nodes.push(node.clone());
            }
        }
    }
    res_nodes
}

// TODO: Adhere to bar groupings e.g. 3+3+3 or 4+4+4
// TODO: Must have all of the time signatures and the full position_in_piece in order
// to tell what should happen when a node overlaps one or more barlines (like a very
// long rest).
fn durations_from_compound(
    length: &Fraction,
    time_signatures: &[TimeSignature],
    position: &PositionInPiece,
) -> Vec<Fraction> {
    // TODO: lengths can only be so small, implement a limit
    // Only numerators of 1 or 3 (dotted note) or 7 (double dotted note) are allowed for lilypond export. Therefore, split other numerators into several notes tied together.
    let mut lengths: Vec<Fraction> = vec![];
    // find out how long of a note can fit within the current grouping
    // convert time signature groupings to fractions
    let mut current_bar_number = position.bar_number as usize;
    let time_signature = &time_signatures[current_bar_number];
    fn get_time_signature_groupings(time_signature: &TimeSignature) -> Vec<Fraction> {
        time_signature
            .groupings
            .iter()
            .map(|g| Fraction::new(*g, time_signature.denominator))
            .collect()
    }
    let mut fraction_groupings = get_time_signature_groupings(&time_signature);
    let mut grouping_index = 0;
    let mut length_left = length.clone();
    // check how much time is left in the current grouping:
    // move to the starting bar position in terms of grouping
    let mut space_in_grouping = fraction_groupings[0] - position.bar_position;

    while length_left > Fraction::zero() {
        // Move forwards, getting new groupings in new bars as needed. This both
        // moves forward to the position in the bar and then allocates new
        // groupings on subsequent loops if the space_in_grouping reaches 0.
        while space_in_grouping <= Fraction::zero() {
            grouping_index += 1;
            if grouping_index >= fraction_groupings.len() {
                current_bar_number += 1;
                fraction_groupings =
                    get_time_signature_groupings(&time_signatures[current_bar_number]);
                grouping_index = 0;
            }
            space_in_grouping += fraction_groupings[grouping_index];
        }
        // fill the space_in_grouping, then go on to the next grouping
        let usable_space = if *space_in_grouping.numer().unwrap() == 1
            || *space_in_grouping.numer().unwrap() == 3
        {
            space_in_grouping.clone()
        } else {
            Fraction::new(1u64, *space_in_grouping.denom().unwrap())
        };
        space_in_grouping -= usable_space;
        // use up all the usable space before going to the next grouping
        // if the note won't fit in the usable space, add it as one length and go on to the next grouping
        if usable_space < length_left {
            if usable_space < Fraction::from(1.0 / 256.0) {
                panic!("usable space is too small: {:?}, space_in_grouping: {:?}, length: {:?}, bar_position: {:?}", usable_space, space_in_grouping, length, position.bar_position);
            }
            lengths.push(usable_space.clone());
            length_left -= usable_space;
        } else {
            // the length_left will fit within the usable space
            while length_left > Fraction::zero() {
                if *length_left.numer().unwrap() == 1 || *length_left.numer().unwrap() == 3 {
                    if length_left < Fraction::from(1.0 / 256.0) {
                        panic!("length_left is too small: {:?}", length_left);
                    }
                    lengths.push(length_left.clone());
                    length_left = Fraction::zero();
                } else {
                    // add as large parts as possible
                    let added_length = get_lilypondable_fractional_part(&length_left);
                    if added_length < Fraction::from(1.0 / 256.0) {
                        panic!("added_length is too small: {:?}", added_length);
                    }
                    lengths.push(added_length);
                    length_left -= added_length;
                }
            }
        }
    }

    lengths
}

/// Recursively find the biggest fraction smaller than original that
/// has a numerator of 1 or 3 or 7
/// TODO: This should try to get note values with one in the numerator first
fn get_lilypondable_fractional_part(original: &Fraction) -> Fraction {
    if *original.numer().unwrap() == 1
        || *original.numer().unwrap() == 3
        || *original.numer().unwrap() == 7
    {
        original.clone()
    } else {
        get_lilypondable_fractional_part(&Fraction::new(
            *original.numer().unwrap() - 1,
            *original.denom().unwrap(),
        ))
    }
}
